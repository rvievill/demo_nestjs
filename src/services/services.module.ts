import { RepositoriesModule } from '@app/repositories/repositories.module';
import { Module } from '@nestjs/common';
import { DemoService } from './demo.service';

@Module({
    imports: [RepositoriesModule],
    controllers: [],
    providers: [DemoService],
    exports: [DemoService]
})
export class ServicesModule {}
