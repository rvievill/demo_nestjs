import { DemoRepository } from '@app/repositories/demo.repository';
import { UserDto } from '@app/shared/dtos/demo.dto';
import { User, UserDb } from '@app/shared/interfaces/demo.interface';
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';

@Injectable()
export class DemoService {
  constructor(private readonly demoRepository: DemoRepository) { }

  public getUsers(age?: number): Array<User> {
    const users = this.demoRepository.getUsers(age);

    return users.map(user => 
      this.formatUser(user)
    );
  }

  public getUser(id: string): User {
    const user = this.demoRepository.getUser(id);
    if (!user) {
      throw new HttpException('user not found', HttpStatus.NOT_FOUND);
    }

    return this.formatUser(user);
  }

  public deleteUser(id: string): User {
    const user = this.demoRepository.deleteUser(id);

    return this.formatUser(user);
  }

  public createUser(user: UserDto): User {
    const userDb = this.demoRepository.createUser(user);

    return this.formatUser(userDb);
  }

  private formatUser(user: UserDb): User {
    return {
      id: user.id,
      name: `${user.firstName} ${user.lastName}`,
      isMajor: user.age >= 18
    }
  } 
}
