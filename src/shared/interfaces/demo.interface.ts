export interface UserDb {
    id: string;
    firstName: string;
    lastName: string;
    age: number;
};

export interface User {
    id: string;
    name: string;
    isMajor: boolean;
}