import { IsBoolean, IsNumber, IsOptional, IsString, MaxLength } from "class-validator";

export class UserDto {
    @IsString()
    @MaxLength(6)
    firstName: string;

    @IsString()
    @MaxLength(10, { message: 'lastName should have max 10 characters'})
    lastName: string;

    @IsNumber()
    age: number;
}