import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DemoGuard } from './shared/guards/demo.guard';
import { DemoInterceptor } from './shared/interceptors/demo.interceptor';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalGuards(new DemoGuard())
  app.useGlobalInterceptors(new DemoInterceptor())
  app.useGlobalPipes(new ValidationPipe({skipMissingProperties: true, transform: true, whitelist: true}));
  app.setGlobalPrefix('api')
  await app.listen(3000);
}
bootstrap();
