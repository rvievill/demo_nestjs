import { Bind, Body, Controller, Delete, Get, Param, Post, Query } from '@nestjs/common';
import { DemoService } from '@app/services/demo.service';
import { User } from '@app/shared/interfaces/demo.interface';
import { UserDto } from '@app/shared/dtos/demo.dto';

@Controller('demo')
export class DemoController {
  constructor(private readonly appService: DemoService) {}

  @Get()
  @Bind(Query('age'))
  getAll(age?: number): Array<User> {
    return this.appService.getUsers(age);
  }

  @Get(':id')
  @Bind(Param('id'))
  getOne(id: string): User {
    return this.appService.getUser(id);
  }

  @Delete(':id')
  @Bind(Param('id'))
  deleteOne(id: string): User {
    return this.appService.deleteUser(id);
  }

  @Post()
  @Bind(Body())
  create(body: UserDto): User {
    console.log(body);
    return this.appService.createUser(body);
  }
}
