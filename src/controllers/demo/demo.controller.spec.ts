import { Test, TestingModule } from '@nestjs/testing';
import { DemoController } from './demo.controller';

describe('AppController', () => {
  let appController: DemoController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [DemoController],
      providers: [],
    }).compile();

    appController = app.get<DemoController>(DemoController);
  });

  describe('root', () => {
    xit('should return "Hello World!"', () => {
    });
  });
});
