import { Module } from '@nestjs/common';

import { DemoController } from './demo/demo.controller';
import { ServicesModule } from '@app/services/services.module';

@Module({
    imports: [ServicesModule],
    controllers: [DemoController],
    providers: [],
})
export class ControllerModule {}
