import { RepositoriesModule } from './repositories/repositories.module';
/*
https://docs.nestjs.com/modules
*/

import { Module } from '@nestjs/common';
import { ControllerModule } from './controllers/controllers.module';
import { ServicesModule } from './services/services.module';

@Module({
    imports: [
        RepositoriesModule,
        ServicesModule,
        ControllerModule
    ],
    controllers: [],
    providers: [],
})
export class AppModule { }
