import { Module } from '@nestjs/common';
import { DemoRepository } from './demo.repository';

@Module({
    imports: [],
    controllers: [],
    providers: [DemoRepository],
    exports: [DemoRepository]
})
export class RepositoriesModule { }
