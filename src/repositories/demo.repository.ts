/*
https://docs.nestjs.com/providers#services
*/

import { UserDto } from '@app/shared/dtos/demo.dto';
import { User, UserDb } from '@app/shared/interfaces/demo.interface';
import { HttpException, HttpStatus, Injectable, NotFoundException } from '@nestjs/common';
import { randomUUID } from 'crypto';
import { NotFoundError } from 'rxjs';

@Injectable()
export class DemoRepository {
    constructor() {}

    private users: Array<UserDb> = [
        {
            id: '0',
            firstName: 'nasi',
            lastName: 'lemak',
            age: 14
        },
        {
            id: '1',
            firstName: 'maggi',
            lastName: 'goreng',
            age: 20
        },
        {
            id: '2',
            firstName: 'beef',
            lastName: 'randeng',
            age: 18
        },
    ]

    public getUsers(age?: number): Array<UserDb> {
        if (age) {
            return this.users.filter(user => user.age === age);
        }
        return this.users;
    }

    public getUser(id: string): UserDb {
        return this.users.find(user => user.id === id);
    }

    public deleteUser(id: string): UserDb {
        const user = this.users.find(user => user.id === id);
        this.users = this.users.filter(user => user.id !== id);

        return user;
    }

    public createUser(user: UserDto): UserDb {
        const id = Math.random().toString(36).substr(2, 9);
        const userDb: UserDb = { ...user, id };

        this.users.push(userDb);

        return userDb;
    }
}
